/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.demo.service;

import java.util.Properties;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

import org.amdatu.jpa.demo.DemoService;
import org.amdatu.jta.ManagedTransactional;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		
		Properties props = new Properties();
		props.put(ManagedTransactional.SERVICE_PROPERTY, DemoService.class.getName());

		//You can either register a component with marker interface ManagedTransactional, or with a service property ManagedTransaction.SERVICE_PROPERTY. 
		//Both will be picked up by the transaction manager aspect.
		dm.add(createComponent().setInterface(Object.class.getName(), props)
				.setImplementation(DemoServiceImpl.class)
				.add(createServiceDependency().setService(EntityManager.class).setRequired(true))
				.add(createServiceDependency().setService(DataSource.class).setRequired(true)));

	}
}
